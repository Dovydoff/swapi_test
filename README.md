# QA Užduotis

Programuotojas buvo paprašytas atlikti užduotį pagal nupieštą [dizainą](https://www.figma.com/file/OjQLB6bckUA69uAcVlUQFP/U%C5%BEduotis?node-id=0%3A1)

### Jūsų pagrindinė užduotis:
- Parsisiųsti ir pasileisti programuotojo sukurtą projektą
- Patikrinti ar jis atitinka dizainą, jei ne naudojantis agail user storie methodologija aprašyt bugus word faile
- Surasti visus įmanomus patobulinimus ir juos surašyti naudojantis agail methodologija į user stories word faile
- Suradus visus įmanomus patobulinimus ir problemas, kurias paliko programuotojas atsiųsti elpaštu: justascesnauskas7@gmail.com

### Papildomo nebūtina užduotis:

- Parašyti automated test (pasirinkta programa nesvarbu) duomenų pridėjimui ir paieškai
